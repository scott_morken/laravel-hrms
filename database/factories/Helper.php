<?php

namespace Database\Factories\Smorken\Hrms\Models\Eloquent;

use Database\Factories\Smorken\Hrms\Models\Eloquent\Helpers\PerOrgHelper;
use Database\Factories\Smorken\Hrms\Models\Eloquent\Helpers\PersonHelper;
use Database\Factories\Smorken\Hrms\Models\Eloquent\Helpers\StatusHelper;
use Database\Factories\Smorken\Hrms\Models\Eloquent\Helpers\TypeHelper;
use Faker\Generator;
use Illuminate\Support\Str;

class Helper
{
    protected array $defaults = [
        'collegeId' => 'COLL01',
    ];

    protected Increment $increment;

    public function __construct(protected Generator $faker, ?Increment $increment = null, array $defaults = [])
    {
        if (is_null($increment)) {
            $increment = new Increment;
        }
        $this->increment = $increment;
        foreach ($defaults as $k => $v) {
            $this->setByKey($k, $v);
        }
    }

    public function getByKey(string $key, mixed $default = null): mixed
    {
        return $this->defaults[$key] ?? $default;
    }

    public function getCollegeId(): string
    {
        return $this->getByKey('collegeId', 'COLL01');
    }

    public function getDeptId(bool $increment = false): int
    {
        return $this->getIncrementing('deptId', $increment ? 1 : 0);
    }

    public function getHrmsId(bool $increment = false): int
    {
        return $this->getIncrementing('hrmsId', $increment ? 1 : 0);
    }

    public function getIncrement(): Increment
    {
        return $this->increment;
    }

    public function getIncrementing(string $key, int $incrementBy = 1): int
    {
        return $this->getIncrement()->getIncrementing($key, $incrementBy);
    }

    public function getJobCode(bool $increment = false): int
    {
        return $this->getIncrementing('jobCode', $increment ? 1 : 0);
    }

    public function getLocation(bool $increment = false): int
    {
        return $this->getIncrementing('location', $increment ? 1 : 0);
    }

    public function getMeid(): string
    {
        $counts = [3, 5];
        $count = $counts[array_rand($counts)];
        $str = strtoupper(Str::random($count));
        $digitCount = 10 - strlen($str);

        return $str.$this->faker->randomNumber($digitCount);
    }

    public function getPerOrgHelper(string $code = PerOrgHelper::EMPLOYEE): PerOrgHelper
    {
        $poh = new PerOrgHelper($this->faker);

        return $poh->get($code);
    }

    public function getPersonHelper(array $properties = []): PersonHelper
    {
        $ph = new PersonHelper($this->faker);

        return $ph->get($properties);
    }

    public function getPositionNumber(bool $increment = false): int
    {
        return $this->getIncrementing('positionNumber', $increment ? 1 : 0);
    }

    public function getSequence(bool $increment = false): int
    {
        return $this->getIncrementing('sequence', $increment ? 1 : 0);
    }

    public function getStatusHelper(string $status = StatusHelper::STATUS_ACTIVE): StatusHelper
    {
        $sh = new StatusHelper($this->faker);

        return $sh->get($status);
    }

    public function getStudentId(bool $increment = false): int
    {
        return $this->getIncrementing('studentId', $increment ? 1 : 0);
    }

    public function getTypeHelper(string $code = TypeHelper::SALARY): TypeHelper
    {
        $th = new TypeHelper($this->faker);

        return $th->get($code);
    }

    public function setByKey(string $key, mixed $value): static
    {
        $this->defaults[$key] = $value;

        return $this;
    }
}
