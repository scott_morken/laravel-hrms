<?php

namespace Database\Factories\Smorken\Hrms\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Hrms\Models\Eloquent\Hcm;

class HcmFactory extends Factory
{
    protected $model = Hcm::class;

    public function definition(): array
    {
        $h = new Helper($this->faker);
        $ph = $h->getPersonHelper();
        $poh = $h->getPerOrgHelper();
        $sh = $h->getStatusHelper();
        $th = $h->getTypeHelper();

        return [
            'MC_HCM_EMPLID' => $h->getHrmsId(),
            'CAMPUS_ID' => $h->getStudentId(),
            'MC_MEID' => $h->getMeid(),
            'NAME' => $ph->name,
            'LAST_NAME' => $ph->lastName,
            'FIRST_NAME' => $ph->firstName,
            'MIDDLE_NAME' => '',
            'NAME_SUFFIX' => '',
            'MC_PREV_NAME' => '',
            'MC_PREF_NAME' => $ph->preferredFirstName,
            'MC_OFFICE_LOC' => '',
            'BUSINESS_UNIT' => $h->getCollegeId(),
            'MC_BU_DESCR' => 'College '.$h->getCollegeId(),
            'MC_BUS_EMAIL' => $ph->email,
            'SEX' => $this->faker->randomElement(['M', 'F', 'N']),
            'MC_FACULTY_IND' => $this->faker->randomElement(['A', 'F', 'N', 'H']),
            'MC_BUS_PHONE' => $this->faker->phoneNumber,
            'EMPL_RCD' => $h->getSequence(),
            'POI_TYPE' => '',
            'PER_ORG' => $poh->code,
            'EFFDT' => date('Y-m-d H:i:s', strtotime('-10 days')),
            'EMPL_STATUS' => $sh->statusCode,
            'ACTION' => $sh->actionCode,
            'ACTION_REASON' => '',
            'ACTION_DT' => date('Y-m-d H:i:s', strtotime('-10 days')),
            'DEPTID' => $h->getDeptId(),
            'MC_DEPT_DESCR' => 'Department '.$h->getDeptId(),
            'JOBCODE' => $h->getJobCode(),
            'MC_JOB_DESCR' => 'Job '.$h->getJobCode(),
            'POSITION_NBR' => $h->getPositionNumber(),
            'MC_POSN_DESCR' => 'Position '.$h->getPositionNumber(),
            'LOCATION' => $h->getLocation(),
            'MC_LOC_DESCR' => 'Location '.$h->getLocation(),
            'JOB_INDICATOR' => $this->faker->randomElement(['P', 'S']),
            'EMPL_TYPE' => $th->code,
            'GRADE' => $this->faker->randomNumber(3),
            'EXPECTED_END_DATE' => null,
            'REPORTS_TO' => $h->getPositionNumber() + 100,
            'MC_REPORT_TO_ID' => $h->getHrmsId() + 100,
            'HR_STATUS' => 'A',
            'ACCT_CD' => 'NA',
            'EMPL_CTG' => '',
        ];
    }
}
