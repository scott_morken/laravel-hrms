<?php

namespace Database\Factories\Smorken\Hrms\Models\Eloquent\Helpers;

use Smorken\Hrms\Models\Enums\EmployeeStatuses;

/**
 * @property string $statusCode
 * @property string $statusDesc
 * @property string $actionCode
 */
class StatusHelper extends BaseHelper
{
    const STATUS_ACTIVE = 'A';

    const STATUS_ON_LEAVE = 'L';

    const STATUS_RETIRED = 'R';

    const STATUS_TERMINATED = 'T';

    const STATUS_WORK_BREAK = 'W';

    protected array $map = [
        self::STATUS_ACTIVE => [
            'statusCode' => EmployeeStatuses::ACTIVE,
            'statusDesc' => 'Active',
            'actionCode' => 'PAY',
        ],
        self::STATUS_ON_LEAVE => [
            'statusCode' => EmployeeStatuses::LEAVE,
            'statusDesc' => 'On Leave',
            'actionCode' => 'DTA',
        ],
        self::STATUS_RETIRED => [
            'statusCode' => EmployeeStatuses::RETIRED,
            'statusDesc' => 'Retired',
            'actionCode' => 'RET',
        ],
        self::STATUS_TERMINATED => [
            'statusCode' => EmployeeStatuses::TERMINATED,
            'statusDesc' => 'Terminated',
            'actionCode' => 'TER',
        ],
        self::STATUS_WORK_BREAK => [
            'statusCode' => EmployeeStatuses::WORK_BREAK,
            'statusDesc' => 'Short Work Break',
            'actionCode' => 'DTA',
        ],
    ];

    public function get(string $status = self::STATUS_ACTIVE): static
    {
        $this->fromMap($status);

        return $this;
    }
}
