<?php

namespace Database\Factories\Smorken\Hrms\Models\Eloquent\Helpers;

use Smorken\Hrms\Models\Enums\Relationships;

/**
 * @property string $code
 * @property string $desc
 */
class PerOrgHelper extends BaseHelper
{
    const CONTINGENT = 'CWR';

    const EMPLOYEE = 'EMP';

    const POI = 'POI';

    protected array $map = [
        self::EMPLOYEE => [
            'code' => Relationships::EMPLOYEE,
            'desc' => 'Employee',
        ],
        self::CONTINGENT => [
            'code' => Relationships::CONTINGENT_WORKER,
            'desc' => 'Contingent Worker',
        ],
        self::POI => [
            'code' => Relationships::PERSON_OF_INTEREST,
            'desc' => 'Person of Interest',
        ],
    ];

    public function get(string $code = self::EMPLOYEE): static
    {
        $this->fromMap($code);

        return $this;
    }
}
