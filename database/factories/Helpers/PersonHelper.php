<?php

namespace Database\Factories\Smorken\Hrms\Models\Eloquent\Helpers;

use Faker\Generator;

/**
 * @property string $lastName
 * @property string $firstName
 * @property string $preferredFirstName
 * @property string $name
 * @property string $email
 */
class PersonHelper
{
    protected array $attributes = [];

    public function __construct(protected Generator $faker) {}

    public function __get(string $key): ?string
    {
        return $this->attributes[$key] ?? null;
    }

    public function get(array $properties = []): static
    {
        $this->setNameTypeProps($properties);

        return $this;
    }

    protected function setNameTypeProps(array $properties): void
    {
        $lastName = $properties['lastName'] ?? $this->faker->lastName;
        $firstName = $properties['firstName'] ?? $this->faker->firstName;
        $prefFirstName = $properties['preferredFirstName'] ?? '';
        $name = sprintf('%s %s', $prefFirstName ?: $firstName, $lastName);
        $email = strtolower(sprintf('%s.%s@example.edu', $prefFirstName ?: $firstName, $lastName));
        $this->attributes['lastName'] = $lastName;
        $this->attributes['firstName'] = $firstName;
        $this->attributes['preferredFirstName'] = $prefFirstName;
        $this->attributes['name'] = $name;
        $this->attributes['email'] = $email;
    }
}
