<?php

namespace Database\Factories\Smorken\Hrms\Models\Eloquent\Helpers;

use Smorken\Hrms\Models\Enums\EmployeeTypes;

/**
 * @property string $code
 * @property string $desc
 */
class TypeHelper extends BaseHelper
{
    const EXCEPTION = 'E';

    const HOURLY = 'H';

    const NA = 'N';

    const SALARY = 'S';

    protected array $map = [
        self::EXCEPTION => [
            'code' => EmployeeTypes::EXCEPTION_HOURLY,
            'desc' => 'Exception Hourly',
        ],
        self::HOURLY => [
            'code' => EmployeeTypes::HOURLY,
            'desc' => 'Hourly',
        ],
        self::SALARY => [
            'code' => EmployeeTypes::SALARIED,
            'desc' => 'Salaried',
        ],
        self::NA => [
            'code' => EmployeeTypes::NOT_APPLICABLE,
            'desc' => 'Not Applicable',
        ],
    ];

    public function get(string $code = self::SALARY): static
    {
        $this->fromMap($code);

        return $this;
    }
}
