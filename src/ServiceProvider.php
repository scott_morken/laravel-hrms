<?php

namespace Smorken\Hrms;

use Illuminate\Contracts\Foundation\Application;
use Smorken\Hrms\Helpers\JobCodes;
use Smorken\Hrms\Models\Eloquent\Job;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        Job::setJobCodes($this->app[JobCodes::class]);
    }

    public function register(): void
    {
        $this->app->scoped(JobCodes::class, function (Application $app) {
            $jobCodes = $app['config']->get('sm-hrms.job_codes', []);

            return new JobCodes($jobCodes);
        });
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'sm-hrms');
        $this->publishes([$config => config_path('sm-hrms.php')], 'config');
    }
}
