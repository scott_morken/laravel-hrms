<?php

namespace Smorken\Hrms\Attributes\Modifiers;

use Smorken\Hrms\Contracts\Models\Hcm;
use Smorken\Model\Attributes\Helpers\Modifiers;

class HcmModifiers extends Modifiers
{
    protected function getFirstName(Hcm $model): string
    {
        $name = trim($model->MC_PREF_NAME ?? '');
        if (! $name) {
            $name = trim($model->FIRST_NAME ?? '');
        }

        return $name;
    }

    protected function getLastName(Hcm $model): string
    {
        return trim($model->LAST_NAME ?? '');
    }

    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'firstName' => fn (mixed $v, Hcm $model): string => $this->getFirstName($model),
                'lastName' => fn (mixed $v, Hcm $model): string => $this->getLastName($model),
            ],
            'set' => [],
        ];
    }
}
