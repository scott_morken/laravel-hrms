<?php

declare(strict_types=1);

namespace Smorken\Hrms\Attributes\AttributeNames\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class JobAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'EMPL_RCD',
            'hcmId' => 'MC_HCM_EMPLID',
            'poiTypeCode' => 'POI_TYPE',
            'relationshipCode' => 'PER_ORG',
            'statusCode' => 'EMPL_STATUS',
            'effectiveDate' => 'EFFDT',
            'actionCode' => 'ACTION',
            'actionReasonCode' => 'ACTION_REASON',
            'actionDate' => 'ACTION_DT',
            'departmentId' => 'DEPTID',
            'departmentDescription' => 'MC_DEPT_DESCR',
            'jobCode' => 'JOBCODE',
            'jobDescription' => 'MC_JOB_DESCR',
            'positionNumber' => 'POSITION_NBR',
            'positionDescription' => 'MC_POSN_DESCR',
            'locationCode' => 'LOCATION',
            'locationDescription' => 'MC_LOC_DESCR',
            'primaryOrSecondary' => 'JOB_INDICATOR',
            'categoryCode' => 'EMPL_CTG',
            'employeeType' => 'EMPL_TYPE',
            'grade' => 'GRADE',
            'expectedEndDate' => 'EXPECTED_END_DATE',
            'reportsToPositionNumber' => 'REPORTS_TO',
            'reportToId' => 'MC_REPORT_TO_ID',
            'accountCode' => 'ACCT_CD',
        ];
    }
}
