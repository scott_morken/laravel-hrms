<?php

declare(strict_types=1);

namespace Smorken\Hrms\Attributes\AttributeNames\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class HcmAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'MC_HCM_EMPLID',
            'altId' => 'MC_MEID',
            'studentId' => 'CAMPUS_ID',
            'statusCode' => 'EMPL_STATUS',
            'firstName' => 'preferred_first_name',
            'lastName' => 'preferred_last_name',
            'middleName' => 'MIDDLE_NAME',
            'suffix' => 'NAME_SUFFIX',
            'previousName' => 'MC_PREV_NAME',
            'email' => 'MC_BUS_EMAIL',
            'phone' => 'MC_BUS_PHONE',
            'primaryLocationCode' => 'BUSINESS_UNIT',
            'primaryLocationDescription' => 'MC_BU_DESCR',
            'facultyCode' => 'MC_FACULTY_IND',
            'hrStatusCode' => 'HR_STATUS',
        ];
    }
}
