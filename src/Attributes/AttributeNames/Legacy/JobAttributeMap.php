<?php

declare(strict_types=1);

namespace Smorken\Hrms\Attributes\AttributeNames\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class JobAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'EMPL_RCD',
            'emplid' => 'MC_HCM_EMPLID',
            'poi_type' => 'POI_TYPE',
            'per_org' => 'PER_ORG',
            'effective_date' => 'EFFDT',
            'action' => 'ACTION',
            'action_reason' => 'ACTION_REASON',
            'action_date' => 'ACTION_DT',
            'status' => 'EMPL_STATUS',
            'department_id' => 'DEPTID',
            'department_desc' => 'MC_DEPT_DESCR',
            'job_code' => 'JOBCODE',
            'job_desc' => 'MC_JOB_DESCR',
            'position_number' => 'POSITION_NBR',
            'position_desc' => 'MC_POSN_DESCR',
            'location_code' => 'LOCATION',
            'location_desc' => 'MC_LOC_DESCR',
            'primary_secondary' => 'JOB_INDICATOR',
            'category' => 'EMPL_CTG',
            'type' => 'EMPL_TYPE',
            'grade' => 'GRADE',
            'expected_end_date' => 'EXPECTED_END_DATE',
            'reports_to_position_number' => 'REPORTS_TO',
            'reports_to_id' => 'MC_REPORT_TO_ID',
            'account_code' => 'ACCT_CD',
        ];
    }
}
