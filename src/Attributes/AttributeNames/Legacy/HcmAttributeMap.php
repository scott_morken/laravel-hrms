<?php

declare(strict_types=1);

namespace Smorken\Hrms\Attributes\AttributeNames\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class HcmAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'MC_HCM_EMPLID',
            'alt_id' => 'MC_MEID',
            'student_id' => 'CAMPUS_ID',
            'status' => 'EMPL_STATUS',
            'hr_status' => 'HR_STATUS',
            'firstName' => 'preferred_first_name',
            'lastName' => 'preferred_last_name',
            'middle_name' => 'MIDDLE_NAME',
            'suffix' => 'NAME_SUFFIX',
            'previous_name' => 'MC_PREV_NAME',
            'email' => 'MC_BUS_EMAIL',
            'phone' => 'MC_BUS_PHONE',
            'sex' => 'SEX',
            'faculty_code' => 'MC_FACULTY_IND',
            'primary_location_code' => 'BUSINESS_UNIT',
            'primary_location_desc' => 'MC_BU_DESCR',
        ];
    }
}
