<?php

namespace Smorken\Hrms\Helpers;

final class Range
{
    public function __construct(
        public int $start,
        public int $end
    ) {}

    public static function from(array $range): self
    {
        return new self(...$range);
    }

    public function in(int $num, bool $inclusive = true): bool
    {
        if ($inclusive) {
            return $num >= $this->start && $num <= $this->end;
        }

        return $num > $this->start && $num < $this->end;
    }
}
