<?php

namespace Smorken\Hrms\Helpers;

class JobCode
{
    public ?Range $range = null;

    public function __construct(
        public string $name,
        public array $list = [],
        Range|array|null $range = null
    ) {
        if ($range) {
            $this->setRange($range);
        }
    }

    public static function fromArray(array $jobCode): self
    {
        return new self(...$jobCode);
    }

    public function has(int $value): bool
    {
        if ($this->range && $this->range->in($value)) {
            return true;
        }

        return in_array($value, $this->list);
    }

    protected function setRange(Range|array $range): void
    {
        if (is_array($range)) {
            $range = Range::from($range);
        }
        $this->range = $range;
    }
}
