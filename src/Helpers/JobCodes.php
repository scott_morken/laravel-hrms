<?php

namespace Smorken\Hrms\Helpers;

use Illuminate\Support\Collection;

class JobCodes
{
    public Collection $jobCodes;

    public function __construct(iterable $jobCodes)
    {
        $this->setJobCodes($jobCodes);
    }

    public function addJobCode(array|JobCode $jobCode): void
    {
        if (is_array($jobCode)) {
            $jobCode = JobCode::fromArray($jobCode);
        }
        $this->jobCodes->push($jobCode);
    }

    public function find(int $value): ?JobCode
    {
        /** @var \Smorken\Hrms\Helpers\JobCode $jobCode */
        foreach ($this->jobCodes as $jobCode) {
            if ($jobCode->has($value)) {
                return $jobCode;
            }
        }

        return null;
    }

    public function findByType(string $type, int $value): ?JobCode
    {
        /** @var \Smorken\Hrms\Helpers\JobCode $jobCode */
        foreach ($this->jobCodes as $jobCode) {
            if ($jobCode->name === $type && $jobCode->has($value)) {
                return $jobCode;
            }
        }

        return null;
    }

    protected function setJobCodes(iterable $jobCodes): void
    {
        $this->jobCodes = new Collection;
        foreach ($jobCodes as $jobCode) {
            $this->addJobCode($jobCode);
        }
    }
}
