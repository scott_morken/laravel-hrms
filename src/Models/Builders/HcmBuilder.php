<?php

namespace Smorken\Hrms\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Hrms\Models\Enums\Actions;
use Smorken\Hrms\Models\Enums\EmployeeStatuses;
use Smorken\Hrms\Models\Enums\FacultyIndicators;
use Smorken\Model\QueryBuilders\Builder;

/**
 * @template TModel of \Smorken\Hrms\Models\Eloquent\Hcm
 * @extends Builder<TModel>
 */
class HcmBuilder extends Builder
{
    public function active(): EloquentBuilder
    {
        return $this->whereIn('EMPL_STATUS', EmployeeStatuses::activeStatuses())
            ->where('ACTION', '<>', Actions::TERMINATE);
    }

    public function altIdIs(string $altId): EloquentBuilder
    {
        return $this->meidIs($altId);
    }

    public function defaultOrder(): EloquentBuilder
    {
        return $this->orderBy('LAST_NAME')
            ->orderBy('FIRST_NAME');
    }

    public function departmentIs(string $deptCode): EloquentBuilder
    {
        return $this->where('DEPTID', '=', $deptCode);
    }

    public function faculty(): EloquentBuilder
    {
        return $this->whereIn('MC_FACULTY_IND', FacultyIndicators::isFaculty());
    }

    public function firstNameLike(string $search): EloquentBuilder
    {
        $search = $search.'%';

        return $this->where(function ($q) use ($search) {
            $q->where('FIRST_NAME', 'LIKE', $search)
                ->orWhere('MC_PREF_NAME', 'LIKE', $search);
        });
    }

    public function idIs(int $id): EloquentBuilder
    {
        return $this->where('MC_HCM_EMPLID', '=', $id);
    }

    public function lastNameLike(string $search): EloquentBuilder
    {
        return $this->where('LAST_NAME', 'LIKE', $search.'%');
    }

    public function location(array|string $locationCodes): EloquentBuilder
    {
        if (is_array($locationCodes)) {
            return $this->locationIn($locationCodes);
        }

        return $this->locationIs($locationCodes);
    }

    public function locationIn(array $locationCodes): EloquentBuilder
    {
        if (! $locationCodes) {
            return $this;
        }

        return $this->whereIn('BUSINESS_UNIT', $locationCodes);
    }

    public function locationIs(string $locationCode): EloquentBuilder
    {
        return $this->where('BUSINESS_UNIT', '=', $locationCode);
    }

    public function meidIs(string $meid): EloquentBuilder
    {
        return $this->where('MC_MEID', '=', $meid);
    }

    public function studentIdIs(int $studentId): EloquentBuilder
    {
        return $this->where('CAMPUS_ID', '=', $studentId);
    }
}
