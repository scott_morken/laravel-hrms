<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/17
 * Time: 7:34 AM
 */

namespace Smorken\Hrms\Models\Eloquent;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Hrms\Attributes\AttributeNames\Camel\JobAttributeMap;
use Smorken\Hrms\Helpers\JobCodes;
use Smorken\Hrms\Models\Enums\JobCodeTypes;
use Smorken\Hrms\Models\Enums\JobIndicators;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Concerns\WithDefaultColumns;
use Smorken\Model\Eloquent;

#[\AllowDynamicProperties]
#[MapAttributeNames(new JobAttributeMap)]
class Job extends Eloquent implements \Smorken\Hrms\Contracts\Models\Job
{
    use Compoships, WithDefaultColumns;

    protected static JobCodes $jobCodes;

    public $incrementing = false;

    public $timestamps = false;

    protected $casts = ['EFFDT' => 'date', 'ACTION_DT' => 'date', 'EXPECTED_END_DATE' => 'date'];

    protected $connection = 'sis';

    protected array $defaultColumns = [
        'EMPL_RCD',
        'MC_HCM_EMPLID',
        'POI_TYPE',
        'PER_ORG',
        'EMPL_STATUS',
        'EFFDT',
        'ACTION',
        'ACTION_REASON',
        'ACTION_DT',
        'DEPTID',
        'MC_DEPT_DESCR',
        'JOBCODE',
        'MC_JOB_DESCR',
        'POSITION_NBR',
        'MC_POSN_DESCR',
        'LOCATION',
        'MC_LOC_DESCR',
        'JOB_INDICATOR',
        'EMPL_CTG',
        'EMPL_TYPE',
        'GRADE',
        'EXPECTED_END_DATE',
        'REPORTS_TO',
        'MC_REPORT_TO_ID',
        'ACCT_CD',
    ];

    protected $table = 'PS_MC_HCM';

    public static function setJobCodes(JobCodes $jobCodes): void
    {
        self::$jobCodes = $jobCodes;
    }

    public function isAdjunct(): bool
    {
        return $this->getJobCodes()->findByType(JobCodeTypes::ADJUNCT, $this->JOBCODE) !== null;
    }

    public function isDepartmentChair(): bool
    {
        return $this->isResidential() && count($this->manages);
    }

    public function isFaculty(): bool
    {
        return $this->isAdjunct() || $this->isResidential();
    }

    public function isManager(): bool
    {
        return $this->manages->isNotEmpty();
    }

    public function isPrimary(): bool
    {
        return $this->JOB_INDICATOR === JobIndicators::PRIMARY;
    }

    public function isResidential(): bool
    {
        return $this->getJobCodes()->findByType(JobCodeTypes::RESIDENTIAL, $this->JOBCODE) !== null;
    }

    public function manager(): BelongsTo
    {
        return $this->belongsTo(
            Hcm::class,
            ['MC_REPORT_TO_ID', 'REPORTS_TO'],
            ['MC_HCM_EMPLID', 'POSITION_NBR']
        );
    }

    public function manages(): HasMany
    {
        return $this->hasMany(
            Hcm::class,
            ['MC_HCM_EMPLID', 'POSITION_NBR'],
            ['MC_REPORT_TO_ID', 'REPORTS_TO']
        );
    }

    protected function getJobCodes(): JobCodes
    {
        return self::$jobCodes;
    }
}
