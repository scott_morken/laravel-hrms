<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/17
 * Time: 7:16 AM
 */

namespace Smorken\Hrms\Models\Eloquent;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HasBuilder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Hrms\Attributes\AttributeNames\Camel\HcmAttributeMap;
use Smorken\Hrms\Models\Builders\HcmBuilder;
use Smorken\Hrms\Models\Concerns\HasPersonNames;
use Smorken\Hrms\Models\Enums\FacultyIndicators;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Concerns\WithDefaultColumns;
use Smorken\Model\Eloquent;

#[\AllowDynamicProperties]
#[MapAttributeNames(new HcmAttributeMap)]
class Hcm extends Eloquent implements \Smorken\Hrms\Contracts\Models\Hcm
{
    use Compoships, HasFactory, HasPersonNames, WithDefaultColumns;

    /** HasBuilder<HcmBuilder<static>> */
    use HasBuilder;

    public $incrementing = false;

    public $timestamps = false;

    protected static string $builder = HcmBuilder::class;

    protected $connection = 'sis';

    protected array $defaultColumns = [
        'MC_HCM_EMPLID',
        'MC_MEID',
        'CAMPUS_ID',
        'EMPL_STATUS',
        'NAME',
        'LAST_NAME',
        'FIRST_NAME',
        'MIDDLE_NAME',
        'NAME_SUFFIX',
        'MC_PREV_NAME',
        'MC_PREF_NAME',
        'MC_BUS_EMAIL',
        'MC_BUS_PHONE',
        'BUSINESS_UNIT',
        'MC_BU_DESCR',
        'SEX',
        'MC_FACULTY_IND',
        'HR_STATUS',
    ];

    protected array $is = [
        'residential' => null,
        'adjunct' => null,
        'manager' => null,
        'dept_chair' => null,
    ];

    protected $table = 'PS_MC_HCM';

    public function isAdjunct(): bool
    {
        if (is_null($this->is['adjunct'])) {
            $this->is['adjunct'] = false;
            foreach ($this->jobs as $job) {
                if ($job->isAdjunct()) {
                    $this->is['adjunct'] = true;
                    break;
                }
            }
        }

        return $this->is['adjunct'];
    }

    public function isDepartmentChair(): bool
    {
        if (is_null($this->is['dept_chair'])) {
            $this->is['dept_chair'] = false;
            foreach ($this->jobs as $job) {
                if ($job->isDepartmentChair()) {
                    $this->is['dept_chair'] = true;
                    break;
                }
            }
        }

        return $this->is['dept_chair'];
    }

    public function isFaculty(): bool
    {
        return in_array($this->MC_FACULTY_IND, FacultyIndicators::isFaculty(), true);
    }

    public function isManager(): bool
    {
        if (is_null($this->is['manager'])) {
            $this->is['manager'] = false;
            $jobs = $this->jobs;
            foreach ($jobs as $job) {
                if ($job->isManager()) {
                    $this->is['manager'] = true;
                    break;
                }
            }
        }

        return $this->is['manager'];
    }

    public function isResidential(): bool
    {
        if (is_null($this->is['residential'])) {
            $this->is['residential'] = false;
            foreach ($this->jobs as $job) {
                if ($job->isResidential()) {
                    $this->is['residential'] = true;
                    break;
                }
            }
        }

        return $this->is['residential'];
    }

    public function jobs(): HasMany
    {
        return $this->hasMany(Job::class, 'MC_HCM_EMPLID', 'MC_HCM_EMPLID');
    }
}
