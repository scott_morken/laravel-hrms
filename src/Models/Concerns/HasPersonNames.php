<?php

declare(strict_types=1);

namespace Smorken\Hrms\Models\Concerns;

trait HasPersonNames
{
    public function fullName(): string
    {
        return $this->getFullNameAttribute();
    }

    public function getFirstName(): string
    {
        $name = trim($this->MC_PREF_NAME ?? '');
        if (! $name) {
            $name = trim($this->FIRST_NAME ?? '');
        }

        return $name;
    }

    public function getFullNameAttribute(): string
    {
        return implode(' ', array_filter([$this->getFirstName(), $this->getLastName()]));
    }

    public function getLastName(): string
    {
        return trim($this->LAST_NAME ?? '');
    }

    public function getPreferredFirstNameAttribute(): string
    {
        return $this->getFirstName();
    }

    public function getPreferredLastNameAttribute(): string
    {
        return $this->getLastName();
    }

    public function getShortNameAttribute(): string
    {
        return implode('. ', array_filter([substr($this->getFirstName(), 0, 1) ?: null, $this->getLastName()]));
    }

    public function shortName(): string
    {
        return $this->getShortNameAttribute();
    }
}
