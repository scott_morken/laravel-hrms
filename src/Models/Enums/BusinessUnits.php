<?php

namespace Smorken\Hrms\Models\Enums;

use Smorken\Hrms\Contracts\Enums\Arrayable;

class BusinessUnits implements Arrayable
{
    public const CGCC = 'GCG08';

    public const EMCC = 'EMC10';

    public const GCC = 'GCC02';

    public const GWCC = 'GWC03';

    public const MCC = 'MCC04';

    public const PCC = 'PCC01';

    public const PVCC = 'PVC09';

    public const RIO = 'RSC06';

    public const SCC = 'SCC05';

    public const SMCC = 'SMC07';

    public static function toArray(): array
    {
        return [
            self::CGCC => 'Chandler Gilbert Community College',
            self::EMCC => 'Estrella Mountain Community College',
            self::GCC => 'Glendale Community College',
            self::GWCC => 'Gateway Community College',
            self::MCC => 'Mesa Community College',
            self::PCC => 'Phoenix College',
            self::PVCC => 'Paradise Valley Community College',
            self::RIO => 'Rio Salado Community College',
            self::SCC => 'Scottsdale Community College',
            self::SMCC => 'South Mountain Community College',
        ];
    }
}
