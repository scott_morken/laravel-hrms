<?php

namespace Smorken\Hrms\Models\Enums;

use Smorken\Hrms\Contracts\Enums\Arrayable;

class FacultyIndicators implements Arrayable
{
    public const CURRENT = 'A';

    public const FUTURE = 'F';

    public const HISTORIC = 'H';

    public const NEVER = 'N';

    public static function isFaculty(): array
    {
        return [
            self::CURRENT,
            self::FUTURE,
            self::HISTORIC,
        ];
    }

    public static function toArray(): array
    {
        return [
            self::CURRENT => 'Currently Faculty',
            self::FUTURE => 'Future Faculty',
            self::HISTORIC => 'Historically Faculty',
            self::NEVER => 'Never Faculty',
        ];
    }
}
