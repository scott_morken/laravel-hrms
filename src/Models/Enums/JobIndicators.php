<?php

namespace Smorken\Hrms\Models\Enums;

use Smorken\Hrms\Contracts\Enums\Arrayable;

class JobIndicators implements Arrayable
{
    public const PRIMARY = 'P';

    public const SECONDARY = 'S';

    public static function toArray(): array
    {
        return [
            self::PRIMARY => 'Primary',
            self::SECONDARY => 'Secondary',
        ];
    }
}
