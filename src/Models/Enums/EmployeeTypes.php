<?php

namespace Smorken\Hrms\Models\Enums;

use Smorken\Hrms\Contracts\Enums\Arrayable;

class EmployeeTypes implements Arrayable
{
    public const EXCEPTION_HOURLY = 'E';

    public const HOURLY = 'H';

    public const NOT_APPLICABLE = 'N';

    public const SALARIED = 'S';

    public static function toArray(): array
    {
        return [
            self::EXCEPTION_HOURLY => 'Exception Hourly',
            self::HOURLY => 'Hourly',
            self::NOT_APPLICABLE => 'Not Applicable',
            self::SALARIED => 'Salaried',
        ];
    }
}
