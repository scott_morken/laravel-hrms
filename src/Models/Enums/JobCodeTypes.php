<?php

namespace Smorken\Hrms\Models\Enums;

class JobCodeTypes
{
    public const ADJUNCT = 'adjunct';

    public const RESIDENTIAL = 'residential';
}
