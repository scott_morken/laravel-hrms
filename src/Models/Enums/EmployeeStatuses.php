<?php

namespace Smorken\Hrms\Models\Enums;

use Smorken\Hrms\Contracts\Enums\Arrayable;

class EmployeeStatuses implements Arrayable
{
    public const ACTIVE = 'A';

    public const LEAVE = 'L';

    public const RETIRED = 'R';

    public const TERMINATED = 'T';

    public const WORK_BREAK = 'W';

    public static function activeStatuses(): array
    {
        return [
            EmployeeStatuses::ACTIVE,
            EmployeeStatuses::LEAVE,
            EmployeeStatuses::WORK_BREAK,
        ];
    }

    public static function toArray(): array
    {
        return [
            self::ACTIVE => 'Active',
            self::LEAVE => 'On Leave',
            self::RETIRED => 'Retired',
            self::TERMINATED => 'Terminated',
            self::WORK_BREAK => 'Short Work Break',
        ];
    }
}
