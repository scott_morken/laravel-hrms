<?php

namespace Smorken\Hrms\Models\Enums;

use Smorken\Hrms\Contracts\Enums\Arrayable;

class Relationships implements Arrayable
{
    public const CONTINGENT_WORKER = 'CWR';

    public const EMPLOYEE = 'EMP';

    public const PERSON_OF_INTEREST = 'POI';

    public static function toArray(): array
    {
        return [
            self::CONTINGENT_WORKER => 'Contingent Worker',
            self::EMPLOYEE => 'Employee',
            self::PERSON_OF_INTEREST => 'Person of Interest',
        ];
    }
}
