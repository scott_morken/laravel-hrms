<?php

namespace Smorken\Hrms\Contracts\Enums;

interface Arrayable
{
    public static function toArray(): array;
}
