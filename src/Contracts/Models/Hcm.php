<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/17
 * Time: 7:14 AM
 */

namespace Smorken\Hrms\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * @property string $MC_HCM_EMPLID
 * @property string $MC_MEID
 * @property string $CAMPUS_ID
 * @property string $EMPL_STATUS
 * @property string $NAME
 * @property string $LAST_NAME
 * @property string $FIRST_NAME
 * @property string $MIDDLE_NAME
 * @property string $NAME_SUFFIX
 * @property string $MC_PREV_NAME
 * @property string $MC_PREF_NAME
 * @property string $MC_BUS_EMAIL
 * @property string $MC_BUS_PHONE
 * @property string $BUSINESS_UNIT
 * @property string $MC_BU_DESCR
 * @property string $SEX
 * @property string $MC_FACULTY_IND
 * @property string $HR_STATUS
 * @property string $id
 * @property string $altId
 * @property string $studentId
 * @property string $statusCode
 * @property string $firstName
 * @property string $lastName
 * @property string $middleName
 * @property string $suffix
 * @property string $previousName
 * @property string $email
 * @property string $phone
 * @property string $primaryLocationCode
 * @property string $primaryLocationDescription
 * @property string $facultyCode
 * @property string $hrStatusCode
 * @property \Illuminate\Support\Collection<\Smorken\Hrms\Contracts\Models\Job> $jobs
 */
interface Hcm extends Model
{
    public function isAdjunct(): bool;

    public function isDepartmentChair(): bool;

    public function isFaculty(): bool;

    public function isManager(): bool;

    public function isResidential(): bool;
}
