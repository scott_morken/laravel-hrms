<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/20/17
 * Time: 7:06 AM
 */

namespace Smorken\Hrms\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * @property int $EMPL_RCD
 * @property string $MC_HCM_EMPLID
 * @property string $POI_TYPE
 * @property string $PER_ORG
 * @property string $EMPL_STATUS
 * @property \Carbon\Carbon $EFFDT
 * @property string $ACTION
 * @property string $ACTION_REASON
 * @property \Carbon\Carbon $ACTION_DT
 * @property string $DEPTID
 * @property string $MC_DEPT_DESCR
 * @property int $JOBCODE
 * @property string $MC_JOB_DESCR
 * @property string $POSITION_NBR
 * @property string $MC_POSN_DESCR
 * @property string $LOCATION
 * @property string $MC_LOC_DESCR
 * @property string $JOB_INDICATOR
 * @property string $EMPL_CTG
 * @property string $EMPL_TYPE
 * @property string $GRADE
 * @property ?\Carbon\Carbon $EXPECTED_END_DATE
 * @property string $REPORTS_TO
 * @property string $MC_REPORT_TO_ID
 * @property string $ACCT_CD
 * @property int $id
 * @property string $hcmId
 * @property string $poiTypeCode
 * @property string $relationshipCode
 * @property string $statusCode
 * @property \Carbon\Carbon $effectiveDate
 * @property string $actionCode
 * @property string $actionReasonCode
 * @property \Carbon\Carbon $actionDate
 * @property string $departmentId
 * @property string $departmentDescription
 * @property int $jobCode
 * @property string $jobDescription
 * @property string $positionNumber
 * @property string $positionDescription
 * @property string $locationCode
 * @property string $locationDescription
 * @property string $primaryOrSecondary
 * @property string $categoryCode
 * @property string $employeeType
 * @property string $grade
 * @property ?\Carbon\Carbon $expectedEndDate
 * @property string $reportsToPositionNumber
 * @property string $reportToId
 * @property string $accountCode
 * @property \Smorken\Hrms\Contracts\Models\Hcm $manager
 * @property \Illuminate\Support\Collection<\Smorken\Hrms\Contracts\Models\Hcm> $manages
 */
interface Job extends Model
{
    public function isAdjunct(): bool;

    public function isDepartmentChair(): bool;

    public function isFaculty(): bool;

    public function isManager(): bool;

    public function isPrimary(): bool;

    public function isResidential(): bool;
}
