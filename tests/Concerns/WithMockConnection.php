<?php

namespace Tests\Smorken\Hrms\Concerns;

use Illuminate\Database\ConnectionInterface;
use Mockery as m;
use Tests\Smorken\Hrms\Stubs\PDOStub;

trait WithMockConnection
{
    protected \PDOStatement|m\MockInterface|null $statement = null;

    protected function getMockConnection(
        string $connectionClass,
        array $config = [],
        array $methods = []
    ): ConnectionInterface {
        $this->statement = null;
        $this->statement = m::mock(\PDOStatement::class);
        $this->statement->shouldReceive('setFetchMode');
        $pdo = m::mock(PDOStub::class);
        $connection = $this->getMockBuilder($connectionClass)
            ->onlyMethods($methods)
            ->setConstructorArgs([$pdo, '', '', $config])
            ->getMock();
        $connection->enableQueryLog();

        return $connection;
    }

    protected function getMySqlConnectionConfig(): array
    {
        return [
            'driver' => 'sqlsrv',
            'url' => null,
            'host' => '127.0.0.1',
            'port' => '1433',
            'database' => null,
            'username' => '',
            'password' => '',
            'unix_socket' => '',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ];
    }
}
