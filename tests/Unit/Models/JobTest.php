<?php

namespace Tests\Smorken\Hrms\Unit\Models;

use Illuminate\Support\Collection;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Hrms\Models\Eloquent\Hcm;
use Tests\Smorken\Hrms\Stubs\StaticModels\JobModels;
use Tests\Smorken\Hrms\Unit\TestCaseWithMockConnectionResolver;

class JobTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = JobModels::get();
        $sut->setRelation('manages', new Collection);
        $sut->setRelation('manager', null);
        $this->assertEquals([
            'id' => 1,
            'hcmId' => 10000000,
            'poiTypeCode' => '',
            'relationshipCode' => 'EMP',
            'effectiveDate' => $sut->effectiveDate,
            'actionCode' => 'PAY',
            'actionReasonCode' => '',
            'actionDate' => $sut->actionDate,
            'statusCode' => 'A',
            'departmentId' => 100,
            'departmentDescription' => 'Department 100',
            'jobCode' => 1000,
            'jobDescription' => 'Job 1000',
            'positionNumber' => 100000,
            'positionDescription' => 'Position 100000',
            'locationCode' => 100,
            'locationDescription' => 'Location 100',
            'primaryOrSecondary' => 'S',
            'categoryCode' => '',
            'employeeType' => 'S',
            'grade' => 513,
            'expectedEndDate' => null,
            'reportsToPositionNumber' => 100100,
            'reportToId' => 10000100,
            'accountCode' => 'NA',
            'manager' => null,
            'manages' => [],
            'CAMPUS_ID' => 30000000,
            'MC_MEID' => 'OP6SP50494',
            'NAME' => 'Gino Miller',
            'LAST_NAME' => 'Miller',
            'FIRST_NAME' => 'Gino',
            'MIDDLE_NAME' => '',
            'NAME_SUFFIX' => '',
            'MC_PREV_NAME' => '',
            'MC_PREF_NAME' => '',
            'MC_OFFICE_LOC' => '',
            'BUSINESS_UNIT' => 'COLL01',
            'MC_BU_DESCR' => 'College COLL01',
            'MC_BUS_EMAIL' => 'gino.miller@example.edu',
            'SEX' => 'M',
            'MC_FACULTY_IND' => 'F',
            'MC_BUS_PHONE' => '+1-575-505-1694',
            'HR_STATUS' => 'A',
        ], $sut->toArray());
    }

    #[Test]
    public function it_returns_bool_for_adjunct_using_model(): void
    {
        $sut = JobModels::get(['JOBCODE' => 4018]);
        $sut->setRelation('manages', new Collection);
        $sut->setRelation('manager', null);
        $this->assertTrue($sut->isAdjunct());
    }

    #[Test]
    public function it_returns_bool_for_dept_char_using_model(): void
    {
        $sut = JobModels::get(['JOBCODE' => 6301]);
        $sut->setRelation('manages', new Collection([
            Hcm::factory()->make(),
        ]));
        $sut->setRelation('manager', null);
        $this->assertTrue($sut->isDepartmentChair());
    }

    #[Test]
    public function it_returns_bool_for_manager_using_model(): void
    {
        $sut = JobModels::get();
        $sut->setRelation('manages', new Collection([
            Hcm::factory()->make(),
        ]));
        $sut->setRelation('manager', null);
        $this->assertTrue($sut->isManager());
    }

    #[Test]
    public function it_returns_bool_for_residential_using_model(): void
    {
        $sut = JobModels::get(['JOBCODE' => 6301]);
        $sut->setRelation('manages', new Collection);
        $sut->setRelation('manager', null);
        $this->assertTrue($sut->isResidential());
    }
}
