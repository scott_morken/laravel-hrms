<?php

namespace Tests\Smorken\Hrms\Unit\Models;

use Illuminate\Support\Collection;
use PHPUnit\Framework\Attributes\Test;
use Tests\Smorken\Hrms\Stubs\StaticModels\HcmModels;
use Tests\Smorken\Hrms\Unit\TestCaseWithMockConnectionResolver;

class HcmTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = HcmModels::get();
        $sut->setRelation('jobs', new Collection);
        $this->assertEquals([
            'id' => 10000000,
            'altId' => 'OP6SP50494',
            'studentId' => 30000000,
            'statusCode' => 'A',
            'hrStatusCode' => 'A',
            'firstName' => 'Gino',
            'lastName' => 'Miller',
            'middleName' => '',
            'suffix' => '',
            'previousName' => '',
            'email' => 'gino.miller@example.edu',
            'phone' => '1-847-575-1929',
            'primaryLocationCode' => 'COLL01',
            'primaryLocationDescription' => 'College COLL01',
            'facultyCode' => 'A',
            'jobs' => [],
            'NAME' => 'Gino Miller',
            'LAST_NAME' => 'Miller',
            'FIRST_NAME' => 'Gino',
            'MC_PREF_NAME' => '',
            'MC_OFFICE_LOC' => '',
            'SEX' => 'F',
            'EMPL_RCD' => 1,
            'POI_TYPE' => '',
            'PER_ORG' => 'EMP',
            'EFFDT' => '2023-04-17 21:28:57',
            'ACTION' => 'PAY',
            'ACTION_REASON' => '',
            'ACTION_DT' => '2023-04-17 21:28:57',
            'DEPTID' => 100,
            'MC_DEPT_DESCR' => 'Department 100',
            'JOBCODE' => 1000,
            'MC_JOB_DESCR' => 'Job 1000',
            'POSITION_NBR' => 100000,
            'MC_POSN_DESCR' => 'Position 100000',
            'LOCATION' => 100,
            'MC_LOC_DESCR' => 'Location 100',
            'JOB_INDICATOR' => 'P',
            'EMPL_TYPE' => 'S',
            'GRADE' => 235,
            'EXPECTED_END_DATE' => null,
            'REPORTS_TO' => 100100,
            'MC_REPORT_TO_ID' => 10000100,
            'ACCT_CD' => 'NA',
            'EMPL_CTG' => '',
        ], $sut->toArray());
    }
}
