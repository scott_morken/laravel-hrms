<?php

namespace Tests\Smorken\Hrms\Unit;

use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\SqlServerConnection;
use Mockery as m;
use Orchestra\Testbench\TestCase;
use Smorken\Data\ServiceProvider;
use Spatie\LaravelData\LaravelDataServiceProvider;
use Tests\Smorken\Hrms\Concerns\WithMockConnection;

class TestCaseWithMockConnectionResolver extends TestCase
{
    use WithMockConnection;

    protected function getPackageProviders($app): array
    {
        return [
            LaravelDataServiceProvider::class,
            ServiceProvider::class,
            \Smorken\Hrms\ServiceProvider::class,
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $cr = new ConnectionResolver(['sis' => $this->getMockConnection(SqlServerConnection::class)]);
        $cr->setDefaultConnection('sis');
        Model::setConnectionResolver($cr);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
