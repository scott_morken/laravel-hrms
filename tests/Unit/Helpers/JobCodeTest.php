<?php

namespace Tests\Smorken\Hrms\Unit\Helpers;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Smorken\Hrms\Helpers\JobCode;
use Smorken\Hrms\Helpers\Range;

class JobCodeTest extends TestCase
{
    #[DataProvider('providerValues')]
    public function testHasInArray(int $test): void
    {
        $sut = new JobCode('test', range(1, 20));
        $this->assertTrue($sut->has($test));
    }

    #[DataProvider('providerValues')]
    public function testNotHasInArray(int $test): void
    {
        $sut = new JobCode('test', range(11, 30));
        $this->assertFalse($sut->has($test));
    }

    #[DataProvider('providerValues')]
    public function testHasInRange(int $test): void
    {
        $sut = new JobCode('test', range(100, 200), new Range(1, 10));
        $this->assertTrue($sut->has($test));
    }

    #[DataProvider('providerValues')]
    public function testNotHasInRange(int $test): void
    {
        $sut = new JobCode('test', range(100, 200), new Range(11, 20));
        $this->assertFalse($sut->has($test));
    }

    public function testFromArrayFactory(): void
    {
        $sut = JobCode::fromArray(
            [
                'name' => 'residential',
                'list' => [1182],
                'range' => ['start' => 6300, 'end' => 9000],
            ]
        );
        $this->assertEquals('residential', $sut->name);
        $this->assertEquals([1182], $sut->list);
        $this->assertEquals(6300, $sut->range->start);
        $this->assertEquals(9000, $sut->range->end);
    }

    public function testFromArrayFactoryWithNullRange(): void
    {
        $sut = JobCode::fromArray(
            [
                'name' => 'adjunct',
                'list' => [1111],
                'range' => null,
            ]
        );
        $this->assertEquals('adjunct', $sut->name);
        $this->assertEquals([1111], $sut->list);
        $this->assertNull($sut->range);
    }

    public static function providerValues(): iterable
    {
        foreach (range(1, 10) as $i) {
            yield [$i, $i];
        }
    }
}
