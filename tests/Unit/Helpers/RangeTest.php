<?php

namespace Tests\Smorken\Hrms\Unit\Helpers;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Smorken\Hrms\Helpers\Range;

class RangeTest extends TestCase
{
    #[DataProvider('providerRange')]
    public function testInRange(int $test): void
    {
        $sut = new Range(1, 10);
        $this->assertTrue($sut->in($test));
    }

    #[DataProvider('providerRange')]
    public function testOutOfRange(int $test): void
    {
        $sut = new Range(11, 20);
        $this->assertFalse($sut->in($test));
    }

    public function testInclusiveForStart(): void
    {
        $sut = new Range(1, 10);
        $this->assertFalse($sut->in(1, false));
    }

    public function testInclusiveForEnd(): void
    {
        $sut = new Range(1, 10);
        $this->assertFalse($sut->in(10, false));
    }

    public function testFromFactory(): void
    {
        $sut = Range::from(['end' => 10, 'start' => 1]);
        $this->assertEquals(1, $sut->start);
        $this->assertEquals(10, $sut->end);
    }

    public static function providerRange(): iterable
    {
        foreach (range(1, 10) as $i) {
            yield [$i, $i];
        }
    }
}
