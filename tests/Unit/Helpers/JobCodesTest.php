<?php

namespace Tests\Smorken\Hrms\Unit\Helpers;

use PHPUnit\Framework\TestCase;
use Smorken\Hrms\Helpers\JobCodes;

class JobCodesTest extends TestCase
{
    public function testFromArray(): void
    {
        $sut = new JobCodes(
            [
                [
                    'name' => 'residential',
                    'list' => [1182],
                    'range' => ['start' => 6300, 'end' => 9000],
                ],
                [
                    'name' => 'adjunct',
                    'list' => [
                        4018,
                        4022,
                        4023,
                        4025,
                    ],
                    'range' => null,
                ],
            ]
        );
        $this->assertCount(2, $sut->jobCodes);
        $this->assertEquals('residential', $sut->find(1182)->name);
        $this->assertEquals('residential', $sut->find(7001)->name);
        $this->assertEquals('adjunct', $sut->find(4022)->name);
        $this->assertNull($sut->find(4024));
    }
}
