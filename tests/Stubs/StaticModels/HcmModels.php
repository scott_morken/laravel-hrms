<?php

namespace Tests\Smorken\Hrms\Stubs\StaticModels;

use Smorken\Hrms\Contracts\Models\Hcm;

class HcmModels
{
    public static function get(array $attributes = []): Hcm
    {
        return (new \Smorken\Hrms\Models\Eloquent\Hcm)->forceFill([
            'MC_HCM_EMPLID' => 10000000,
            'CAMPUS_ID' => 30000000,
            'MC_MEID' => 'OP6SP50494',
            'NAME' => 'Gino Miller',
            'LAST_NAME' => 'Miller',
            'FIRST_NAME' => 'Gino',
            'MIDDLE_NAME' => '',
            'NAME_SUFFIX' => '',
            'MC_PREV_NAME' => '',
            'MC_PREF_NAME' => '',
            'MC_OFFICE_LOC' => '',
            'BUSINESS_UNIT' => 'COLL01',
            'MC_BU_DESCR' => 'College COLL01',
            'MC_BUS_EMAIL' => 'gino.miller@example.edu',
            'SEX' => 'F',
            'MC_FACULTY_IND' => 'A',
            'MC_BUS_PHONE' => '1-847-575-1929',
            'EMPL_RCD' => 1,
            'POI_TYPE' => '',
            'PER_ORG' => 'EMP',
            'EFFDT' => '2023-04-17 21:28:57',
            'EMPL_STATUS' => 'A',
            'ACTION' => 'PAY',
            'ACTION_REASON' => '',
            'ACTION_DT' => '2023-04-17 21:28:57',
            'DEPTID' => 100,
            'MC_DEPT_DESCR' => 'Department 100',
            'JOBCODE' => 1000,
            'MC_JOB_DESCR' => 'Job 1000',
            'POSITION_NBR' => 100000,
            'MC_POSN_DESCR' => 'Position 100000',
            'LOCATION' => 100,
            'MC_LOC_DESCR' => 'Location 100',
            'JOB_INDICATOR' => 'P',
            'EMPL_TYPE' => 'S',
            'GRADE' => 235,
            'EXPECTED_END_DATE' => null,
            'REPORTS_TO' => 100100,
            'MC_REPORT_TO_ID' => 10000100,
            'HR_STATUS' => 'A',
            'ACCT_CD' => 'NA',
            'EMPL_CTG' => '',
            ...$attributes,
        ]);
    }
}
