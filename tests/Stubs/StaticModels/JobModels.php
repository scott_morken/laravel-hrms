<?php

namespace Tests\Smorken\Hrms\Stubs\StaticModels;

use Smorken\Hrms\Contracts\Models\Hcm;
use Smorken\Hrms\Contracts\Models\Job;

class JobModels
{
    public static function fromHcm(Hcm $hcm, array $attributes = []): Job
    {
        return self::get([
            'MC_HCM_EMPLID' => $hcm->MC_HCM_EMPLID,
            'CAMPUS_ID' => $hcm->CAMPUS_ID,
            'MC_MEID' => $hcm->MC_MEID,
            'NAME' => $hcm->NAME,
            'LAST_NAME' => $hcm->LAST_NAME,
            'FIRST_NAME' => $hcm->FIRST_NAME,
            'MIDDLE_NAME' => $hcm->MIDDLE_NAME,
            'NAME_SUFFIX' => $hcm->NAME_SUFFIX,
            'MC_PREV_NAME' => $hcm->MC_PREV_NAME,
            'MC_PREF_NAME' => $hcm->MC_PREF_NAME,
            'SEX' => $hcm->SEX,
            ...$attributes,
        ]);
    }

    public static function get(array $attributes = []): Job
    {
        return (new \Smorken\Hrms\Models\Eloquent\Job)->forceFill([
            'MC_HCM_EMPLID' => 10000000,
            'CAMPUS_ID' => 30000000,
            'MC_MEID' => 'OP6SP50494',
            'NAME' => 'Gino Miller',
            'LAST_NAME' => 'Miller',
            'FIRST_NAME' => 'Gino',
            'MIDDLE_NAME' => '',
            'NAME_SUFFIX' => '',
            'MC_PREV_NAME' => '',
            'MC_PREF_NAME' => '',
            'MC_OFFICE_LOC' => '',
            'BUSINESS_UNIT' => 'COLL01',
            'MC_BU_DESCR' => 'College COLL01',
            'MC_BUS_EMAIL' => 'gino.miller@example.edu',
            'SEX' => 'M',
            'MC_FACULTY_IND' => 'F',
            'MC_BUS_PHONE' => '+1-575-505-1694',
            'EMPL_RCD' => 1,
            'POI_TYPE' => '',
            'PER_ORG' => 'EMP',
            'EFFDT' => '2023-04-17 21:34:59',
            'EMPL_STATUS' => 'A',
            'ACTION' => 'PAY',
            'ACTION_REASON' => '',
            'ACTION_DT' => '2023-04-17 21:34:59',
            'DEPTID' => 100,
            'MC_DEPT_DESCR' => 'Department 100',
            'JOBCODE' => 1000,
            'MC_JOB_DESCR' => 'Job 1000',
            'POSITION_NBR' => 100000,
            'MC_POSN_DESCR' => 'Position 100000',
            'LOCATION' => 100,
            'MC_LOC_DESCR' => 'Location 100',
            'JOB_INDICATOR' => 'S',
            'EMPL_TYPE' => 'S',
            'GRADE' => 513,
            'EXPECTED_END_DATE' => null,
            'REPORTS_TO' => 100100,
            'MC_REPORT_TO_ID' => 10000100,
            'HR_STATUS' => 'A',
            'ACCT_CD' => 'NA',
            'EMPL_CTG' => '',
            ...$attributes,
        ]);
    }
}
