<?php

return [
    'job_codes' => [
        [
            'name' => \Smorken\Hrms\Models\Enums\JobCodeTypes::RESIDENTIAL,
            'list' => [1182],
            'range' => ['start' => 6300, 'end' => 9000],
        ],
        [
            'name' => \Smorken\Hrms\Models\Enums\JobCodeTypes::ADJUNCT,
            'list' => [
                4018, //Music Instruction Hrly
                4022, //NC Instr Hrly
                4023, //NC Instr Hrly Other
                4025, //Service Fac Hrly Day
                4027, //Service Fac Hrly Evening
                4028, //Substitute Pay Day
                4029, //Substitute Pay Evening
                4091, //Nursing Clinical Instructor
                4092, //Nursing Lab Instructor
                4108, //Fire Recruit Instructor
                4111, //Police Recruit Instructor
                4204, //Adj Day
                4205, //Adj Eve
                4206, //Adj Summer Day
                4207, //Adj Summer Eve
                4212, //Service Fac Adj Day
                4218, //Service Fac Adj Eve
            ],
            'range' => null,
        ],
    ],
];
